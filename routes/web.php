<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [\App\Http\Controllers\TransportistaController::class,"index"])->name("transportistas.index");

Route::get('transportistas', [\App\Http\Controllers\TransportistaController::class,"index"])->name("transportistas.index");

Route::get('transportistas/{transportista}', [\App\Http\Controllers\TransportistaController::class,"show"])->name("transportistas.show");

Route::get('transportistas/{transportista}/entregar', [\App\Http\Controllers\TransportistaController::class,"entregar"])->name("transportistas.entregar");

Route::get('transportistas/{transportista}/noEntregar', [\App\Http\Controllers\TransportistaController::class,"noEntregar"])->name("transportistas.noEntregar");

Route::get('paquetes/crear', [\App\Http\Controllers\PaqueteController::class,"create"])->name("paquetes.crear");

Route::post('paquetes/{paquete}',[\App\Http\Controllers\PaqueteController::class,'store'])->name("paquetes.store");
