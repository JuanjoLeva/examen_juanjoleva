@extends("layouts.master")

@section("titulo")
    Crear
@endsection

@section("contenido")
    <div class="row">
        <div class="offset-md-3 col-md-6">
            <div class="card">
                <div class="card-header text-center">
                    <h3>Crear paquete</h3>
                </div>
                <div class="card-body" style="padding:30px">
                    <form method="post" action="" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="especie"><strong>Direccion</strong></label>
                            <input type="text" name="direccion" id="direccion" class="form-control" required>
                            <br><br>
                        </div>
                        @foreach($transportistas as $transportista)
                        <div class="form-group">
                            <strong>Transportista</strong> <option value="$transportista->nombre"> {{$transportista->nombre}}</option>
                            <br><br>
                        </div>
                        @endforeach
                        <div class="form-group">
                            <strong>Introduce una imagen</strong> <br> <input type="file" name="imagen">
                            <br><br>
                        </div>
                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-warning" style="padding:8px 100px;margin-top:25px;">
                                Añadir receta
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
