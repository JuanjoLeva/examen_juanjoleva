@extends("layouts.master")

@section("titulo")
    Transportista
@endsection

@section("contenido")
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <img src="{{asset('assets/imagenes/transportistas')}}/{{$transportista->imagen}}" class="rounded-circle border border-primary" style="height:275px;width:200px"/>
            </div>

            <div class="col-sm-9">
                <h3 class="align-center" style="min-height:45px;margin:5px 0 10px 0" >
                    {{$transportista->nombre}}
                </h3>
                <li><strong>Años carnet:</strong> {{$transportista->getEdad()}}</li>
                <br><br>

                <li><strong>Empresas:</strong></li>
                @foreach($transportista->empresas as $empresa)
                    <ul>Nombre: {{$empresa->nombre}}</ul>
                @endforeach<br>

                @if(count($transportista->paquetes)>0)

                <li><strong>Paquetes:</strong></li>
                @foreach($transportista->paquetes as $paquete)
                    <ul>Paquete {{$paquete->id}} -- {{$paquete->direccionEntrega}} @if($paquete->entregado==false) <strong>paquete no entregado</strong> @elseif($paquete->entregado==true) <strong>paquete entregado</strong> @endif</ul>
                @endforeach<br>

                @endif

                <a href="{{ route('transportistas.entregar',$transportista) }}"><button class="btn btn-warning">Entregar todo</button></a>
                <a href="{{ route('transportistas.noEntregar',$transportista) }}"><button class="btn btn-success">Marcar todo como no entregado</button></a>
            </div>
        </div>
    </div>
@endsection
