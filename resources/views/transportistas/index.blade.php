@extends("layouts.master")

@section("titulo")
    Transportistas
@endsection

@section("contenido")
    <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
        <div class="container">
            <div class="row">
                @foreach( $transportistas as $clave => $transportista )
                    <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12" style="margin:30px 10px 0">
                        <div class="card-body w-50  mx-auto d-block">
                            <a href="{{ route('transportistas.show', $transportista->slug) }}">
                                <img class=" border border-warning img-fluid" src="{{asset('assets/imagenes/transportistas')}}/{{$transportista->imagen}}" style="height:200px;width:250px"/>
                            </a>
                            <h4 class="text-center" style="min-height:45px;margin:5px 0 10px 0">
                                {{$transportista['nombre']}}
                            </h4>
                            <h6 class="text-center" style="min-height:45px;margin:5px 0 10px 0">
                                Paquetes sin entregar--{{count($transportista->paquetes->where("entregado","=",false))}}
                            </h6>
                            <br>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </main>
@endsection
