<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transportista extends Model
{
    use HasFactory;
    protected $guarded=[];

    public function empresas()
    {
        return $this->belongsToMany(Empresa::class);
    }

    public function paquetes()
    {
        return $this->hasMany(Paquete::class);
    }

    public function getEdad()
    {
        $fechaFormateada=Carbon::parse($this->fechaPermisoConducir);
        return $fechaFormateada->diffInYears(Carbon::now());
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

}

