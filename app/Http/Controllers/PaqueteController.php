<?php

namespace App\Http\Controllers;

use App\Models\Paquete;
use App\Models\Transportista;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PaqueteController extends Controller
{
    public function create()
    {
        $transportistas=Transportista::all();
        return view("paquetes.create", compact("transportistas"));
    }

    public function store(Request $request){

        $datos=$request->all();
        $datos['imagen']=$request->imagen->store('','paquetes');

        $nuevoPaquete=Paquete::create($datos);

        $transportista=Transportista::findOrFail($nuevoPaquete->transportista_id);

        return view("transportistas.show",compact("transportista"));
    }
}
