<?php

namespace App\Http\Controllers;

use App\Models\Transportista;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TransportistaController extends Controller
{
    public function index(){
        $transportistas=Transportista::all();
        return view("transportistas.index",compact("transportistas"));
    }

    public function show(Transportista $transportista)
    {
        return view("transportistas.show",compact("transportista"));
    }

    public function entregar(Transportista $transportista)
    {
        $paquetes=DB::table('paquetes')->get();

        foreach ($paquetes as $paquete){
            if($paquete->transportista_id == $transportista->id){
                DB::table("paquetes")->where("transportista_id",'=',$transportista->id)->update(array('entregado' => true));
            }
        }

        return redirect()->route('transportistas.show',compact("transportista"))->with('mensaje','Se han puesto en entregado los paquetes');
    }

    public function noEntregar(Transportista $transportista)
    {
        $paquetes=DB::table('paquetes')->get();

        foreach ($paquetes as $paquete){
            if($paquete->transportista_id == $transportista->id){
                DB::table("paquetes")->where("transportista_id",'=',$transportista->id)->update(array('entregado' => false));
            }
        }

        return redirect()->route('transportistas.show',compact("transportista"))->with('mensaje','Se han puesto en NO entregado los paquetes');
    }
}
